<?php

namespace App\Controller;

use App\Entity\Item;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Event\Model\OrderStatusEvent;
use App\Form\OrderType;
use App\Services\OrderService;
use App\Utils\Constants;
use App\Utils\OrderState;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class APIController extends Controller {


    protected $orderService;
    public function __construct(EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher, ValidatorInterface $validator,UrlGeneratorInterface $urlGenerator,OrderService $orderService)
    {
        parent::__construct($entityManager, $eventDispatcher, $validator,$urlGenerator);
        $this->orderService = $orderService;
    }


    /**
     * @Route("/api/order/create",methods={"POST"})
     */
    public function createOrder(Request $request): JsonResponse
    {

        $data = json_decode($request->getContent(),true);

        $constraints = Constants::getCreateOrderConstraints();
        $violations = $this->validator->validate($data,$constraints);

        if (0 !== count($violations)) {
            return $this->errorResponse(JsonResponse::HTTP_UNPROCESSABLE_ENTITY,$this->getValidationErrors($violations));
        }

        $response = $this->orderService->createOrder($data);

        if ($response["error"]) {
            return $this->errorResponse($response["code"],$response["message"]);
        }

        return $this->successResponse([
            "message" => $response["message"],
            "order" => $response["data"]
        ]);

    }

    /**
     * @Route("/api/order/cancel",methods={"PATCH"})
     */
    public function cancelOrder(Request $request): JsonResponse
    {

        $data = json_decode($request->getContent(),true);

        $constraints = Constants::getCancelOrderConstraints();
        $violations = $this->validator->validate($data,$constraints);

        if (0 !== count($violations)) {
            return $this->errorResponse(JsonResponse::HTTP_UNPROCESSABLE_ENTITY,$this->getValidationErrors($violations));
        }

        $response = $this->orderService->cancelOrder($data["orderId"]);

        if ($response["error"]) {
            return $this->errorResponse($response["code"],$response["message"]);
        }

        return $this->successResponse([
            "message" => $response["message"],
            "order_id" => $response["data"]
        ]);

    }



}