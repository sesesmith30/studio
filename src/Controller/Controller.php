<?php


namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Controller extends AbstractController {



    protected $entityManager;
    protected $encoders;
    protected $normalizers;
    protected $serializer;
    protected $dispatcher;
    protected $validator;
    protected $urlGenerator;


    public function __construct(EntityManagerInterface $entityManager,EventDispatcherInterface $eventDispatcher,ValidatorInterface $validator,UrlGeneratorInterface $urlGenerator)
    {
        $this->entityManager = $entityManager;
        $this->encoders = [new XmlEncoder(), new JsonEncoder()];
        $this->normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($this->normalizers, $this->encoders);
        $this->dispatcher = $eventDispatcher;
        $this->validator = $validator;
        $this->urlGenerator = $urlGenerator;
    }


    /**
     * get the error from forms
     * @param FormInterface $form
     * @return array
     */
    public function getFormErrorMessages(FormInterface $form): array
    {
        $formName = $form->getName();
        $errors = [];

        foreach ($form->getErrors(true, true) as $formError) {
            $name = '';
            $thisField = $formError->getOrigin()->getName();
            $origin = $formError->getOrigin();
            while ($origin = $origin->getParent()) {
                if ($formName !== $origin->getName()) {
                    $name = $origin->getName() . '_' . $name;
                }
            }
            $fieldName = $formName . '_' . $name . $thisField;
            /**
             * One field can have multiple errors
             */
            if (!in_array($fieldName, $errors)) {
                $errors[$fieldName] = [];
            }
            $errors[$fieldName][] = $formError->getMessage();
        }

        return $errors;
    }

    public function getValidationErrors($violations) {
        $errors = [];
        foreach ($violations as $key => $violation) {
            array_push($errors,$violation->getMessage()." - ".$violation->getPropertyPath());
        }

        return $errors;
    }

    public function errorResponse($code,$message): JsonResponse
    {
        return new JsonResponse([
            "error" => true,
            "message" => $message
        ],$code);
    }

    public function successResponse($data): JsonResponse {
        return new JsonResponse([
            "error" => false,
            "data" => $data
        ]);
    }


}