<?php

namespace App\Controller;
use App\Controller\Controller;
use App\Entity\Admin;
use App\Entity\Item;
use App\Entity\Order;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ItemController extends Controller {


    /**
     * @Route("/api/items",name="items",methods={"GET"})
     */
    public function index() {

        $items = $this->entityManager->getRepository(Item::class)->findAll();

        $items = (new ArrayCollection(json_decode($this->serializer->serialize($items, 'json'),true)))->map(function($item) {
            $item["createdAt"] = $item["createdAt"]["timestamp"];
            $item["updatedAt"] = $item["updatedAt"]["timestamp"];
            return $item;
        })->toArray();

        return $this->successResponse([
            "items" => $items,//
            "count" => sizeof($items)
        ]);
    }


    /**
     * @Route("/api/item",name="add_item",methods={"POST"})
     */
    public function addItem(Request $request): JsonResponse
    {

        $data = json_decode($request->getContent(),true);

        if (!isset($data["name"]) || !isset($data["price"])) {
            return $this->errorResponse(JsonResponse::HTTP_UNPROCESSABLE_ENTITY,"required field is empty");
        }

        $item = new Item();
        $item->setName($data["name"]);
        $item->setPrice($data["price"]);
        $item->setCreatedAt(new \DateTime());
        $item->setUpdatedAt(new \DateTime());
        $this->entityManager->persist($item);
        $this->entityManager->flush();

        return $this->successResponse(["message" => "added successfully","item_id" => $item->getId()]);

    }

    /**
     * @Route("/api/item/{id}",name="item_by_id",methods={"GET"})
     */
    public function getItemById(Request $request,$id){


        $item = $this->entityManager->getRepository(Item::class)->find($id);

        if (!isset($item)) {
            return $this->errorResponse(JsonResponse::HTTP_FORBIDDEN,"no item found with this id");
        }

        $item = (new ArrayCollection(json_decode($this->serializer->serialize($item, 'json'),true)))->toArray();
        $item["createdAt"] = $item["createdAt"]["timestamp"];
        $item["updatedAt"] = $item["updatedAt"]["timestamp"];

        return $this->successResponse([
            "item" => $item
        ]);

    }

    /**
     * @Route("/api/item/{id}/edit",name="edit_item_by_id",methods={"PUT"})
     */
    public function editItem(Request $request,$id): JsonResponse
    {

        $item = $this->entityManager->getRepository(Item::class)->findBy($id);

        $data = json_decode($request->getContent(),true);

        if (!isset($item)) {
            return $this->errorResponse(JsonResponse::HTTP_FORBIDDEN,"no item found with this id");
        }

        foreach ($data as $key => $value) {
            $item->{$key} = $value;
        }
        $item->setUpdatedAt(new \DateTime());
        $this->entityManager->flush();

        return $this->successResponse(["message" => "item updated successfully"]);

    }

    /**
     * @Route("/api/item/{id}",name="delete_item_by_id",methods={"DELETE"})
     */
    public function deleteItem(Request $request,$id) {

        $item = $this->entityManager->getRepository(Item::class)->find($id);

        if (!isset($item)) {
            return $this->errorResponse(JsonResponse::HTTP_FORBIDDEN,"no item found with this id");
        }

        $this->entityManager->remove($item);
        $this->entityManager->flush();

        return $this->successResponse(["message" => "deleted successfully"]);

    }





}