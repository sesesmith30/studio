<?php


namespace App\Controller;
use App\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Order;
use App\Services\OrderService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ManagementOrderController extends Controller {


    protected $orderService;

    public function __construct(EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher, ValidatorInterface $validator,UrlGeneratorInterface $urlGenerator,OrderService $orderService)
    {
        parent::__construct($entityManager, $eventDispatcher, $validator,$urlGenerator);
        $this->orderService = $orderService;
    }

    /**
     * @Route("/management/orders", name="management_orders")
     */
    public function orders(Request $request): Response
    {

        $len = $request->query->get("len");
        $state = $request->query->get("state");
        $orderId = $request->query->getInt("order_id");
        $page = $request->query->getInt("page");

        $criteria = [];

        if (isset($orderId) && $orderId != 0) {
            $criteria["id"] = $orderId;
        }
        if (isset($state)) {
            $criteria["state"] = $state;
        }

        $len = isset($len) && $len != 0 ? $len : 5;
        $page = isset($page) && $page != 0 ? $page : 1;
        $offset = ($page - 1) * $len;

        $orders = $this->entityManager->getRepository(Order::class)->findBy($criteria,["createdAt" => "DESC"],$len,$offset);
        //total count
        $totalCount = $this->entityManager->getRepository(Order::class)->count($criteria);

        $pagination = [
            "total_count" => isset($totalCount) ? $totalCount : 0,
            "len" => $len > $totalCount ? $totalCount : $len
        ];

        return $this->render('management/orders.html.twig',
            [
                "orders" => $orders,
                "params" => $params = $request->query->all(),
                "pagination" => $pagination
            ]
        );


    }

    /**
     * @Route("/management/order/{id}/details", name="management_order_details")
     */
    public function orderDetails(Request $request,$id) {

        $details = $this->orderService->getOrderDetails($id);

        return $this->render('management/order.html.twig',
            [
                "order" => $details["order"],
                "orderIssueLog" => $details["orderIssueLog"],
                "orderLogHistory" => $details["orderLogHistory"]
            ]
        );
    }


}
