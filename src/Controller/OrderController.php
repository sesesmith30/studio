<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class OrderController extends Controller {


    /**
     * @Route("/home", name="home")
     */
    public function home(Request $request): RedirectResponse
    {
        $role = $this->getUser()->getRoles()[0];

        if ($role === "ROLE_MANAGEMENT") {
            return new RedirectResponse($this->urlGenerator->generate('management_orders'));
        }else if ($role === "ROLE_SHIPPING") {
            return new RedirectResponse($this->urlGenerator->generate('shipping_orders'));
        }else {
            return new RedirectResponse($this->urlGenerator->generate('picking_orders'));
        }
    }


}