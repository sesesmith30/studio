<?php


namespace App\Controller;
use App\Controller\Controller;
use App\Entity\Order;
use App\Services\PickingOrderServices;
use App\Utils\Constants;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\OrderService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PickingOrderController extends Controller {


    protected $orderService;
    protected $pickingOrderService;

    public function __construct(EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher, ValidatorInterface $validator,UrlGeneratorInterface $urlGenerator,OrderService $orderService,PickingOrderServices $pickingOrderService)
    {
        parent::__construct($entityManager, $eventDispatcher, $validator,$urlGenerator);
        $this->orderService = $orderService;
        $this->pickingOrderService = $pickingOrderService;
    }


    /**
     * @Route("/picking/order/orders", name="picking_orders")
     */
    public function orders(): Response
    {

        $orders = $this->entityManager->getRepository(Order::class)->getPickupOrders();

        return $this->render('picking/orders.html.twig',
            [
                "orders" => $orders
            ]
        );
    }

    /**
     * @Route("/picking/order/{id}/details", name="picking_order_details")
     */
    public function orderDetails($id): Response
    {

        $details = $this->orderService->getOrderDetails($id);

        return $this->render('picking/order.html.twig',
            [
                "order" => $details["order"],
                "orderIssueLog" => $details["orderIssueLog"],
                "orderLogHistory" => $details["orderLogHistory"]
            ]
        );
    }


    /**
     * @Route("/picking/order/process",methods={"POST"})
     */
    public function processOrder(Request $request): Response
    {
        $orderId = $request->get("order_id");

        $constraints = Constants::getProcessOrderConstraints();
        $violations = $this->validator->validate([
            "order_id" => $orderId
        ],$constraints);

        if (0 !== count($violations)) {
            $errors = $this->getValidationErrors($violations);
            $this->addFlash("error",$errors[0]);
            return $this->redirectToRoute("picking_orders");
        }


        $response = $this->pickingOrderService->processOrder($orderId,$this->getUser());

        if ($response["error"]) {
            $this->addFlash("error",$response["message"]);
        }else {
            $this->addFlash("success","Order processed successfully");
        }

        return $this->redirectToRoute("picking_order_details",["id" => $orderId]);

    }



    /**
     * @Route("/picking/order/make-ready-to-ship",methods={"POST"})
     */
    public function makeOrderReadyToShip(Request $request)
    {

        $boxId = $request->get("box_id");
        $orderId = $request->get("order_id");

        $constraints = Constants::getMakeOrderReadyConstraints();
        $violations = $this->validator->validate([
            "box_id" => $boxId,
            "order_id" => $orderId
        ],$constraints);

        if (0 !== count($violations)) {
            $errors = $this->getValidationErrors($violations);
            $this->addFlash("error",$errors[0]);
            return $this->redirectToRoute("picking_order_details",["id" => $orderId]);
        }

        $response = $this->pickingOrderService->makeOrderReadyForShipping($orderId,$boxId,$this->getUser());


        if ($response["error"]) {
            $this->addFlash("error",$response["message"]);
        }else {
            $this->addFlash("success","Order processed successfully");
        }


        return $this->redirectToRoute("picking_order_details",["id" => $orderId]);

    }



}
