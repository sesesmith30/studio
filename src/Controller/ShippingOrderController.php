<?php


namespace App\Controller;
use App\Controller\Controller;
use App\Entity\Order;
use App\Services\ShippingOrderServices;
use App\Utils\Constants;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\OrderService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ShippingOrderController extends Controller {


    protected $orderService;
    protected $shippingOrderServices;

    public function __construct(EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher, ValidatorInterface $validator,UrlGeneratorInterface $urlGenerator,OrderService $orderService,ShippingOrderServices $shippingOrderServices)
    {
        parent::__construct($entityManager, $eventDispatcher, $validator,$urlGenerator);
        $this->orderService = $orderService;
        $this->shippingOrderServices = $shippingOrderServices;
    }

    /**
     * @Route("/shipping/orders", name="shipping_orders")
     */
    public function orders(): Response
    {

        $orders = $this->entityManager->getRepository(Order::class)->getShippingOrders();

        return $this->render('shipping/orders.html.twig',
            [
                "orders" => $orders,
            ]
        );

    }

    /**
     * @Route("/shipping/order/{id}/details", name="shipping_order_details")
     */
    public function orderDetails($id): Response
    {

        $details = $this->orderService->getOrderDetails($id);

        return $this->render('shipping/order.html.twig',
            [
                "order" => $details["order"],
                "orderIssueLog" => $details["orderIssueLog"],
                "orderLogHistory" => $details["orderLogHistory"]
            ]
        );
    }

    /**
     * @Route("/shipping/order/report",methods={"POST"})
     */
    public function reportOrder(Request $request): RedirectResponse
    {

        $orderId = $request->get("order_id");
        $issueTitle = $request->get("issue");
        $issueValue = $request->get("issue_text");

        $constraints = Constants::getReportOrderConstraints();
        $violations = $this->validator->validate([
            "order_id" => $orderId,
            "issue_title" => $issueTitle,
            "issue_value" => $issueValue
        ],$constraints);

        if (0 !== count($violations)) {
            $errors = $this->getValidationErrors($violations);
            $this->addFlash("error",$errors[0]);
            return $this->redirectToRoute("shipping_orders");
        }



        $response = $this->shippingOrderServices->reportOrder($orderId,$issueTitle,$issueValue,$this->getUser());


        if ($response["error"]) {
            $this->addFlash("error",$response["message"]);
        }else {
            $this->addFlash("success","Order Reported successfully");
        }

        return $this->redirectToRoute("shipping_order_details",["id" => $orderId]);

    }


    /**
     * @Route("/shipping/order/ship",methods={"POST"})
     */
    public function shipOrder(Request $request) {

        $orderId = $request->get("order_id");
        $company = $request->get("shipping_company");
        $trackingNumber = $request->get("shipping_tracking_number");
        $file = $request->files->get("shipping_label_url");


        $constraints = Constants::getShipOrderConstraints();
        $violations = $this->validator->validate([
            "order_id" => $orderId,
            "shipping_company" => $company,
            "shipping_tracking_number" => $trackingNumber
        ],$constraints);

        if (0 !== count($violations)) {
            $errors = $this->getValidationErrors($violations);
            $this->addFlash("error",$errors[0]);
            return $this->redirectToRoute("shipping_orders");
        }

        $response = $this->shippingOrderServices->shipOrder($orderId,
            $company,
            $trackingNumber,
            $file,
            $this->getParameter("upload_directory"),
            $this->getUser());

        if ($response["error"]) {
            $this->addFlash("error",$response["message"]);
        }else {
            $this->addFlash("success","Order Shipped successfully");
        }

        return $this->redirectToRoute("shipping_order_details",["id" => $orderId]);


    }


}
