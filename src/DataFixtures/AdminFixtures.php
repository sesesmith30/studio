<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminFixtures extends Fixture
{

    private $passwordEncoder;


      private $passwordHasher;

     public function __construct(UserPasswordHasherInterface $passwordHasher)
     {
         $this->passwordHasher = $passwordHasher;
     }


    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);


        //picking
        $admin = new Admin();
        $admin->setUsername("picking01");
        $admin->setPassword($this->passwordHasher->hashPassword(
            $admin, 'picking@2021'));
        $admin->setRoles(["ROLE_PICKING"]);
        $manager->persist($admin);


        //shipping
        $admin = new Admin();
        $admin->setUsername("shipping01");
        $admin->setPassword($this->passwordHasher->hashPassword(
            $admin, 'shipping@2021'));
        $admin->setRoles(["ROLE_SHIPPING"]);
        $manager->persist($admin);

        //management
        $admin = new Admin();
        $admin->setUsername("management01");
        $admin->setPassword($this->passwordHasher->hashPassword(
            $admin, 'management@2021'));
        $admin->setRoles(["ROLE_MANAGEMENT"]);
        $manager->persist($admin);




        $manager->flush();
    }
}
