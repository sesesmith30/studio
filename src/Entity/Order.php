<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use App\Utils\OrderState;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{


    use TimestampableEntity;


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $total;

    /**
     * @ORM\Column(type="float")
     */
    private $discount;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderItem", mappedBy="order")
     */
    private $orderItems;

    /**
     * @ORM\Column(type="text")
     */
    private $shipping_details;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $shipping_company;


    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $shipping_tracking_number;


    /**
     * @ORM\Column(type="text",nullable=true)
     */
    private $shipping_label_url;

    /**
     * @ORM\Column(type="boolean",length=1, nullable=true)
     */
    private $has_issue=false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $box_id=null;


    /**
     * @ORM\Column(type="string")
     */
    private $state=OrderState::ORDER_RECEIVED;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTotal(): ?string
    {
        return $this->total;
    }

    public function setTotal(string $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    public function setDiscount(float $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getOrderItems(): ?Collection
    {
        return $this->orderItems;
    }


    public function getShippingDetails(): ?string
    {
        return $this->shipping_details;
    }

    public function setShippingDetails(string $shipping_details): self
    {
        $this->shipping_details = $shipping_details;

        return $this;
    }

    public function getShippingCompany(): ?string
    {
        return $this->shipping_company;
    }

    public function setShippingCompany(string $shippingCompany): self
    {
        $this->shipping_company = $shippingCompany;

        return $this;
    }

    public function getShippingTrackingNumber(): ?string
    {
        return $this->shipping_tracking_number;
    }

    public function setShippingTrackingNumber(string $shippingTrackingNumber): self
    {
        $this->shipping_tracking_number = $shippingTrackingNumber;

        return $this;
    }

    public function getShippingLabelUrl(): ?string
    {
        return $this->shipping_label_url;
    }

    public function setShippingLabelUrl(string $shippingLabelUrl): self
    {
        $this->shipping_label_url = $shippingLabelUrl;

        return $this;
    }


    public function getHasIssue(): ?bool
    {
        return $this->has_issue;
    }

    public function setHasIssue(bool $hasIssue): self
    {
        $this->has_issue = $hasIssue;
        return $this;
    }

    public function setState(string $state): self
    {
        $this->state = $state;
        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setBoxId(string $boxId): self
    {
        $this->box_id = $boxId;
        return $this;
    }

    public function getBoxId(): ?string
    {
        return $this->box_id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }









}
