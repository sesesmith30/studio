<?php

namespace App\Entity;

use App\Repository\OrderStatusLogRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=OrderStatusLogRepository::class)
 */
class OrderStatusLog
{

    use TimestampableEntity;


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin", inversedBy="orderStatusLog")
     */
    private $admin;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Order", inversedBy="orderStatusLog")
     */
    private $order;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $issue_title;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $issue_value;




    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdmin(): ?Admin
    {
        return $this->admin;
    }

    public function setAdmin(?Admin $admin): self
    {
        $this->admin = $admin;

        return $this;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getIssueTitle(): ?string
    {
        return $this->issue_title;
    }

    public function setIssueTitle(?string $issue_title): self
    {
        $this->issue_title = $issue_title;

        return $this;
    }

    public function getIssueValue(): ?string
    {
        return $this->issue_value;
    }

    public function setIssueValue(?string $issue_value): self
    {
        $this->issue_value = $issue_value;

        return $this;
    }
}
