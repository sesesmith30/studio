<?php

namespace App\Event\Model;
use Symfony\Contracts\EventDispatcher\Event;

class OrderStatusEvent extends Event {

    public const EVENT = "OrderStatusEvent";

    protected $admin;
    protected $order;
    protected $state;
    protected $issueTitle;
    protected $issueValue;

    /**
     * OrderStatusEvent constructor.
     * @param $admin
     * @param $order
     * @param $state
     * @param $issueTitle
     * @param $issueValue
     */
    public function __construct($admin, $order, $state, $issueTitle, $issueValue)
    {
        $this->admin = $admin;
        $this->order = $order;
        $this->state = $state;
        $this->issueTitle = $issueTitle;
        $this->issueValue = $issueValue;
    }



    /**
     * @return mixed
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param mixed $admin
     */
    public function setAdmin($admin): void
    {
        $this->admin = $admin;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order): void
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state): void
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getIssueTitle()
    {
        return $this->issueTitle;
    }

    /**
     * @param mixed $issueTitle
     */
    public function setIssueTitle($issueTitle): void
    {
        $this->issueTitle = $issueTitle;
    }

    /**
     * @return mixed
     */
    public function getIssueValue()
    {
        return $this->issueValue;
    }

    /**
     * @param mixed $issueValue
     */
    public function setIssueValue($issueValue): void
    {
        $this->issueValue = $issueValue;
    }





}