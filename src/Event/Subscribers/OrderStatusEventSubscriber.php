<?php

namespace App\Event\Subscribers;
use App\Entity\OrderStatusLog;
use App\Event\Model\OrderStatusEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderStatusEventSubscriber implements EventSubscriberInterface {

    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    public static function getSubscribedEvents()
    {
        // TODO: Implement getSubscribedEvents() method.
        return [
          OrderStatusEvent::EVENT => "eventStatusChanged"
        ];
    }


    public function eventStatusChanged(OrderStatusEvent $event) {

        //create a new order status log
        $log = new OrderStatusLog();
        $log->setOrder($event->getOrder());
        $log->setState($event->getState());
        $log->setAdmin($event->getAdmin());
        $log->setIssueTitle($event->getIssueTitle());
        $log->setIssueValue($event->getIssueValue());
        $log->setCreatedAt(new \DateTime());
        $log->setUpdatedAt(new \DateTime());

        $this->entityManager->persist($log);
        $this->entityManager->flush();


    }
}