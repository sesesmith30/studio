<?php

namespace App\Repository;

use App\Entity\Order;
use App\Utils\OrderState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

     /**
      * @return Order[] Returns an array of Order objects
      */
    public function getPickupOrders()
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.state = :state or o.state = :state2 or (o.has_issue = :hasIssue and o.state = :state)')
            ->setParameter(':state', OrderState::ORDER_RECEIVED)
            ->setParameter(':state2', OrderState::ORDER_PROCESSING)
            ->setParameter(':hasIssue', true)
            ->orderBy('o.createdAt', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }


    /**
     * @return Order[] Returns an array of Order objects
     */
    public function getShippingOrders()
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.state = :state')
            ->setParameter(':state', OrderState::ORDER_READY_TO_SHIP)
            ->orderBy('o.createdAt', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
