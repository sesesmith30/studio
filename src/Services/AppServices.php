<?php

namespace App\Services;


class AppServices {


    /**
     * Upload resources
     * @param $file
     * @param $directory
     * @return string
     */
    public function uploadResources($file,$directory): string
    {

        $fileName = md5(uniqid()).".".$file->guessExtension();

        $file->move(
            $directory,
            $fileName
        );

        return $fileName;

    }
}