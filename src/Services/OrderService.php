<?php


namespace App\Services;


use App\Entity\Item;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\OrderStatusLog;
use App\Event\Model\OrderStatusEvent;
use App\Utils\OrderState;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OrderService {



    protected $entityManager;
    protected $encoders;
    protected $normalizers;
    protected $serializer;
    protected $dispatcher;
    protected $validator;


    public function __construct(EntityManagerInterface $entityManager,EventDispatcherInterface $eventDispatcher,ValidatorInterface $validator)
    {
        $this->entityManager = $entityManager;
        $this->encoders = [new XmlEncoder(), new JsonEncoder()];
        $this->normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($this->normalizers, $this->encoders);
        $this->dispatcher = $eventDispatcher;
        $this->validator = $validator;
    }



    public function createOrder($data): array
    {

        $order = new Order();

        $total = 0.0;
        foreach ($data["items"] as $item) {
            $itemData = $this->entityManager->getRepository(Item::class)->find($item["item_id"]);

            if (!isset($itemData)) {
                return [
                    "error" => true,
                    "message" => "item with id ".$item["item_id"]." don't exist",
                    "code" => JsonResponse::HTTP_BAD_REQUEST
                ];
            }

            $total = $total + ($item["quantity"] * $itemData->getPrice());
            //create Order Item
            $orderItem = new OrderItem();
            $orderItem->setItem($itemData);
            $orderItem->setQuantity($item["quantity"]);
            $orderItem->setOrder($order);
            $this->entityManager->persist($orderItem);
        }


        $order->setDiscount($data["discount"]);
        $order->setTotal($total);
        $order->setShippingDetails($data["shipping_details"]);
        $order->setCreatedAt(new \DateTime());
        $order->setUpdatedAt(new \DateTime());


        $this->entityManager->persist($order);
        $this->entityManager->flush();

        $orderStatusEvent = new OrderStatusEvent(null,$order,$order->getState(),null,null);
        //dispatch event
        $this->dispatcher->dispatch($orderStatusEvent, OrderStatusEvent::EVENT);

        $mOrder = $this->serializer->normalize($order,null);
        $mOrder["createdAt"] = $mOrder["createdAt"]["timestamp"];
        $mOrder["updatedAt"] = $mOrder["updatedAt"]["timestamp"];


        return [
            "error" => false,
            "data" => $mOrder,
            "message" => "order created successfully"
        ];


    }

    /**
     * Cancel Order
     * @param $orderId
     * @return array
     */
    public function cancelOrder($orderId): array
    {

        $order = $this->entityManager->getRepository(Order::class)->find($orderId);

        if (!isset($order)) {
            return [
                "error" => true,
                "message" => "order not found",
                "code" => JsonResponse::HTTP_FORBIDDEN
            ];
        }

        $order->setState(OrderState::ORDER_CANCELED);
        $this->entityManager->flush();

        return [
            "error" => false,
            "message" => "order cancelled successfully",
        ];

    }

    /**
     * Get Order details
     * @param $id
     * @return array
     */
    public function getOrderDetails($id): array
    {

        $order = $this->entityManager->getRepository(Order::class)->find($id);

        $orderIssueLog = null;
        if ($order->getState() == OrderState::ORDER_PROCESSING && $order->getHasIssue()) {
            //log have an issue
            $orderIssueLog = $this->entityManager
                ->getRepository(OrderStatusLog::class)
                ->findBy([  "order" => $order->getId(),
                    "state" => "ORDER_PROCESSING"
                ],["id" => "DESC"])[0];
        }

        $orderLogHistory = $this->entityManager
            ->getRepository(OrderStatusLog::class)
            ->findBy(["order" => $order->getId()],["createdAt" => "ASC"]);

        return [
            "order" => $order,
            "orderIssueLog" => $orderIssueLog,
            "orderLogHistory" => $orderLogHistory
        ];
    }

}