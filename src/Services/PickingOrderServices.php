<?php

namespace App\Services;


use App\Entity\Admin;
use App\Entity\Order;
use App\Event\Model\OrderStatusEvent;
use App\Utils\OrderState;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PickingOrderServices {



    protected $entityManager;
    protected $encoders;
    protected $normalizers;
    protected $serializer;
    protected $dispatcher;
    protected $validator;


    public function __construct(EntityManagerInterface $entityManager,EventDispatcherInterface $eventDispatcher,ValidatorInterface $validator)
    {
        $this->entityManager = $entityManager;
        $this->encoders = [new XmlEncoder(), new JsonEncoder()];
        $this->normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($this->normalizers, $this->encoders);
        $this->dispatcher = $eventDispatcher;
        $this->validator = $validator;
    }



    public function processOrder($orderId,$user): array
    {
        $admin = $this->entityManager->getRepository(Admin::class)->findOneBy(["username" => $user->getUserIdentifier()]);

        $order = $this->entityManager->getRepository(Order::class)->find($orderId);

        if ($admin->getCurrentOrder() != null) {
            return [
                "error" => true,
                "message" => "user already have an order working on"
            ];
        }

        $admin->setCurrentOrder($order->getId());
        $order->setState(OrderState::ORDER_PROCESSING);


        $this->entityManager->flush();

        //dispatch event change
        $orderStatusEvent = new OrderStatusEvent($admin,$order,$order->getState(),null,null);
        $this->dispatcher->dispatch($orderStatusEvent, OrderStatusEvent::EVENT);

        return [
            "error" => false
        ];

    }

    public function makeOrderReadyForShipping($orderId,$boxId,$user): array
    {

        $admin = $this->entityManager->getRepository(Admin::class)->findOneBy(["username" => $user->getUserIdentifier()]);

        $order = $this->entityManager->getRepository(Order::class)->find($orderId);

        $order->setBoxId($boxId);

        //clear admin current
        $admin->setCurrentOrder(null);
        $order->setState(OrderState::ORDER_READY_TO_SHIP);

        $this->entityManager->flush();

        //dispatch event change
        $orderStatusEvent = new OrderStatusEvent($admin,$order,$order->getState(),null,null);
        $this->dispatcher->dispatch($orderStatusEvent, OrderStatusEvent::EVENT);

        return [
            "error" => false
        ];



    }
}