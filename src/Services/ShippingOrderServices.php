<?php

namespace App\Services;


use App\Entity\Admin;
use App\Entity\Order;
use App\Event\Model\OrderStatusEvent;
use App\Utils\OrderState;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ShippingOrderServices
{


    protected $entityManager;
    protected $encoders;
    protected $normalizers;
    protected $serializer;
    protected $dispatcher;
    protected $validator;
    protected $appServices;


    public function __construct(EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher, ValidatorInterface $validator,AppServices $appServices)
    {
        $this->entityManager = $entityManager;
        $this->encoders = [new XmlEncoder(), new JsonEncoder()];
        $this->normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($this->normalizers, $this->encoders);
        $this->dispatcher = $eventDispatcher;
        $this->validator = $validator;
        $this->appServices = $appServices;
    }



    public function reportOrder($orderId,$title,$value,$user): array
    {

        $admin = $this->entityManager->getRepository(Admin::class)->findOneBy(["username" => $user->getUserIdentifier()]);

        $order = $this->entityManager->getRepository(Order::class)->find($orderId);


        $order->setHasIssue(true);
        $order->setState(OrderState::ORDER_PROCESSING);
        $this->entityManager->flush();

        //dispatch event change
        $orderStatusEvent = new OrderStatusEvent($admin,$order,$order->getState(),$title,$value);
        $this->dispatcher->dispatch($orderStatusEvent, OrderStatusEvent::EVENT);

        return [
            "error" => false
        ];

    }

    public function shipOrder($orderId,$company,$trackingNumber,$file,$uploadDirectory,$user): array
    {


        $admin = $this->entityManager->getRepository(Admin::class)->findOneBy(["username" => $user->getUserIdentifier()]);

        $order = $this->entityManager->getRepository(Order::class)->find($orderId);


        $order->setState(OrderState::ORDER_SHIPPED);
        $order->setShippingCompany($company);
        $order->setShippingTrackingNumber($trackingNumber);

        $resourceUpload = $this->appServices->uploadResources($file,$uploadDirectory);
        $order->setShippingLabelUrl($resourceUpload);

        //dispatch event change
        $orderStatusEvent = new OrderStatusEvent($admin,$order,$order->getState(),null,null);
        $this->dispatcher->dispatch($orderStatusEvent, OrderStatusEvent::EVENT);


        return [
            "error" => false
        ];

    }


}