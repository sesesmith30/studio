<?php

namespace App\Twig;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;


class AppExtension extends AbstractExtension {

    public function getFilters()
    {
        return [
            new TwigFilter('toJSON', [$this, 'toJSON']),
            new TwigFilter('showIfIsset', [$this, 'showIfIsset']),
        ];
    }

    public function toJSON($data)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $data = json_decode($serializer->serialize($data,"json"),true);
        return json_encode($data);
    }

    public function showIfIsset($data) {
        return isset($data) ? $data : '';
    }
}