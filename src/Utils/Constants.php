<?php

namespace App\Utils;

use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class Constants {


     public static function getCreateOrderConstraints(): Collection
     {
         return new Collection([
             "discount" => new NotBlank(),
             "shipping_details" => new NotBlank(),
             "items" =>  [
                 new Type("array"),
                 new All([
                     new Collection([
                         "item_id" => [new NotBlank(),new Type("integer")],
                         "quantity" => [new NotBlank(), new Type("integer")]
                     ])
                 ])
             ]
         ]);
     }

    public static function getMakeOrderReadyConstraints(): Collection
    {
        return new Collection([
            "order_id" => new NotBlank(),
            "box_id" => new NotBlank(),
        ]);
    }

    public static function getProcessOrderConstraints(): Collection
    {
        return new Collection([
            "order_id" => new NotBlank()
        ]);
    }

    public static function getShipOrderConstraints(): Collection
    {
        return new Collection([
            "order_id" => new NotBlank(),
            "shipping_company" => new NotBlank(),
            "shipping_tracking_number" => new NotBlank()
        ]);
    }

    public static function getReportOrderConstraints(): Collection
    {
        return new Collection([
            "order_id" => new NotBlank(),
            "issue_title" => new NotBlank(),
            "issue_value" => new NotBlank()
        ]);
    }

    public static function getCancelOrderConstraints(): Collection
    {
        return new Collection([
            "item_id" => new NotBlank(),
        ]);
    }

}