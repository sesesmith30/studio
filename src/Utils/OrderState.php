<?php

namespace App\Utils;

abstract class OrderState {
    const ORDER_RECEIVED = "ORDER_RECEIVED";
    const ORDER_CANCELED = "ORDER_CANCELED";
    const ORDER_PROCESSING = "ORDER_PROCESSING";
    const ORDER_READY_TO_SHIP = "ORDER_READY_TO_SHIP";
    const ORDER_SHIPPED = "ORDER_SHIPPED";
}